### Complex Numbers in C# ###


            ComplexNumber x = new ComplexNumber(1, 1);
            Console.WriteLine(x.ToString());
            Console.WriteLine(x.ToStringPolar());
            Console.WriteLine(ComplexNumber.Negation(x).ToString());

            Console.WriteLine(ComplexNumber.Multiply(new ComplexNumber(5, 6), new ComplexNumber(3, 4)));