﻿using System;

namespace ComplexNumbers
{
    public class ComplexNumber
    {
        //Object functions and properties

        public double realpart;
        public double imaginarypart;

        public ComplexNumber(double real_part, double imaginary_part) {
            this.realpart = real_part;
            this.imaginarypart = imaginary_part;
        }

        public override string ToString() {
            string sign = this.imaginarypart > 0 ? "+" : "-";
            return string.Format("{0}{1}{2}i", this.realpart, sign, Math.Abs(this.imaginarypart));
        }

        public string ToStringPolar()
        {
            double angle = Math.Atan2(Math.Abs(this.imaginarypart), Math.Abs(this.realpart)) * (180 / Math.PI);
            double magnitude = Math.Sqrt(Convert.ToDouble(this.imaginarypart * this.imaginarypart + this.realpart * this.realpart));
            return string.Format("{0}<{1}", magnitude, angle);
        }

        //Static functions

        static public ComplexNumber Add(ComplexNumber number1, ComplexNumber number2)
        {
            double realpartAddition = number1.realpart + number2.realpart;
            double imaginarypartAddition = number1.imaginarypart + number2.imaginarypart;

            return new ComplexNumber(realpartAddition, imaginarypartAddition);
        }

        static public ComplexNumber Add(ComplexNumber[] numbers)
        {
            double realpartAddition = 0;
            double imaginarypartAddition = 0;

            foreach (ComplexNumber number in numbers)
            {
                realpartAddition += number.realpart;
                imaginarypartAddition += number.imaginarypart;
            }

            return new ComplexNumber(realpartAddition, imaginarypartAddition);
        }



        static public ComplexNumber Subtract(ComplexNumber number1, ComplexNumber number2)
        {
            double realpartSubtraction = number1.realpart - number2.realpart;
            double imaginarypartSubtraction = number1.imaginarypart - number2.imaginarypart;

            return new ComplexNumber(realpartSubtraction, imaginarypartSubtraction);
        }

        static public ComplexNumber Subtract(ComplexNumber[] numbers)
        {
            double realpartSubtraction = 0;
            double imaginarypartSubtraction = 0;

            foreach (ComplexNumber number in numbers)
            {
                realpartSubtraction += number.realpart;
                imaginarypartSubtraction += number.imaginarypart;
            }

            return new ComplexNumber(realpartSubtraction, imaginarypartSubtraction);
        }



        static public ComplexNumber Negation(ComplexNumber number)
        {
            double realpartNegated = 0 - number.realpart;
            double imaginarypartNegated = 0 - number.imaginarypart;

            return new ComplexNumber(realpartNegated, imaginarypartNegated);
        }



        static public ComplexNumber Multiply(ComplexNumber number1, ComplexNumber number2)
        {

            double realpart = number1.realpart * number2.realpart - (number1.imaginarypart * number2.imaginarypart);
            double imaginarypart = number1.imaginarypart * number2.realpart + number1.realpart * number2.imaginarypart;

            return new ComplexNumber(realpart, imaginarypart);
        }
    }
}
